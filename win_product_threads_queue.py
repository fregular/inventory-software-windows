from elasticsearch import Elasticsearch
import os
import winrm
import json
from datetime import datetime
import time
import calendar
import threading
import queue

queue = queue.Queue()
running = False

ES_SERVER = os.getenv('ES_SERVER','http://127.0.0.1:9200')
ES_INDEX = os.getenv('ES_INDEX','product')
ES_INDEX_QUERY = os.getenv('ES_INDEX_QUERY','gole-nmap-parsed_*')
LISTMAPUSER = os.getenv('MAPUSER', 'localhost/_winmap,domain/Administrator').split(',')
LISTMAPPASS = os.getenv('MAPPASS', 'password1,password2').split(',')
THREAD_COUNT = int(os.getenv('THREAD_COUNT','10'))

def query_elasticsearch(query,servers=None):
    if query == 1:
        query = {'size':3000,
         '_source': 'false',
         'sort': [{'g_last_mod_date': {'order': 'desc'}}],
         'query': {'bool': {'must': [{'match': {'map_type': 'wintel'}},
            {'match': {'parsed': 0}},
            {'match': {'ports': '5985'}},
            {"wildcard":{"os_version":"*server*"}},
            {'range': {'g_last_mod_date': {'gte': 'now-1d/d'}}}]}},
         'aggs': {'hostname': {'terms': {'field': 'hostname', 'size': 3000}}}}

    if query == 2:
        query = {
            "size": 3000,
            "_source":["wmi_user","hostname","UserName","g_businessunit","g_country","g_flag","g_local","os_version","physical_address","role","tenant"],
            "sort":[{"g_last_mod_date": {"order": "desc"}}],
            "query":{
                "constant_score": {
                    "filter": {
                        "bool": {
                            "must": [{
                        "terms":{"hostname": servers }},
                        {
                            "range":{
                            "g_last_mod_date": {
                                "gte" : "now-1d/d"}}}]}}}}}

    if query == 3:
        query = {
            'size': 3000,
            '_source': 'false',
            'sort': [{'g_last_mod_date': {'order': 'desc'}}],
            'query': {'range': {'g_last_mod_date': {'gte': 'now-1d/d'}}},
            'aggs': {'hostname': {'terms': {'field': 'hostname', 'size': 3000}}}}

    return query

def query_elastic():
    res = es.search(index=ES_INDEX_QUERY, body=query_elasticsearch(1), request_timeout=120)
    servers = []
    servers2 = []
    for hostname in res['aggregations']['hostname']['buckets']:
        servers.append(hostname['key'])
    if "Monday" not in day:
        try:
            res3 = es.search(index=ES_INDEX, body=query_elasticsearch(3), request_timeout=120)
            for hostname in res3['aggregations']['hostname']['buckets']:
                servers2.append(hostname['key'].upper())
            servers = list(set(servers) - set(servers2))
        except:
            pass
    res2 = es.search(index=ES_INDEX_QUERY, body=query_elasticsearch(2, servers=servers), request_timeout=120)
    data_server = []
    for host in res2['hits']['hits']:
        data_server.append(host['_source'])

    data_server = remove_dupe_dicts(data_server)
    return data_server
    
def get_product(server):
    #print(server)
    index = LISTMAPUSER.index(server['wmi_user'])
    password = LISTMAPPASS[index]
    lista = []
    try:
        s = winrm.Session(server['hostname'],auth=(server['wmi_user'], password), transport='ntlm')
        result1 = s.run_cmd('reg', ['query', 'HKLM\\Software\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall','/s','/t','REG_SZ'])
        result2 = s.run_cmd('reg', ['query', 'HKLM\\Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall','/s','/t','REG_SZ'])
        result1 = parsed_out(result1.std_out.decode("utf-8", "ignore").split('\r\n'))
        result2 = parsed_out(result2.std_out.decode("utf-8", "ignore").split('\r\n'))
        all_products = result1 + result2
        all_products = remove_dupe_dicts(all_products)
        for product in all_products:
            final_parsed = {** server, ** product}
            lista.append(final_parsed)
        insert_elastic(lista)
    except:
        fail = []
        server['parsed'] = 3
        server['g_last_mod_date'] = g_last_mod_date()
        server['method'] = "winrm"
        fail.append(server)
        insert_elastic(fail)

def parsed_out(product_list):
    parsed = []
    a = None
    for i in product_list:
        if i == '':
            continue
        if a == None:
            a = {}
        if "DisplayVersion" in i:
            a['Version'] =i.split('    ')[3]
        if "Publisher" in i:
            a['Vendor'] = i.split('    ')[3]
        if " DisplayName" in i:
            a['Name'] = i.split('    ')[3]
        if "ParentDisplayName" in i:
            a['ParentName'] = i.split('    ')[3]
        if "InstallDate" in i:
            a['InstallDate'] = i.split('    ')[3]
        if 'HKEY_LOCAL' in i or 'End of search' in i:
            if len(a) == 0:
                continue
            else:
                a['parsed'] = 0
                a['g_last_mod_date'] = g_last_mod_date()
                a['method'] = "winrm"
                parsed.append(a)
                a = None
                #continue
    return parsed

def g_last_mod_date():
    epoch = int(datetime.timestamp(datetime.now()) * 1000)
    return epoch

def insert_elastic(lista):
    for doc_server in lista:
        es.index(index=ES_INDEX,body=doc_server,request_timeout=120)

def remove_dupe_dicts(l):
  list_of_strings = [
    json.dumps(d, sort_keys=True)
    for d in l
  ]
  list_of_strings = set(list_of_strings)
  return [
    json.loads(s)
    for s in list_of_strings
  ]

def findDay(date):
    born = datetime.strptime(date, '%d %m %Y').weekday()
    return (calendar.day_name[born])

def worker():
    while running:
        try:
            item = queue.get()
            if item is None:
                break
            try:
                get_product(item)
            finally:
                queue.task_done()
        except queue.Empty:
            pass


print('starting inventory-software!!')
print(datetime.now())
today =time.strftime("%d %m %Y")
day = findDay(today)
print(day)

es = Elasticsearch(ES_SERVER)
result = query_elastic()
print("amount of devices = " + str(len(result)))

list_thread = []
if __name__ == '__main__':
    running = True
    for i in range(THREAD_COUNT):
        t = threading.Thread(target=worker)
        #t.daemon = True
        t.start()
        list_thread.append(t)

    # add servers in the queue
    for server in result:
        queue.put(server)

    # wait for all items to finish processing
    queue.join()

    # Signal all threads to break and join the main thread 
    for x in range(THREAD_COUNT):
        queue.put(None)
    for thread in list_thread:
        thread.join()

    running = False

print('finish inventory-software!!')
print(datetime.now())
